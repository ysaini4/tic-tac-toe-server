// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;
var users = {};
server.listen(port, () => {
  console.log('Server listening at port %d', port);
});
app.use(express.static(path.join(__dirname+'/../', 'tic-tac-toe')));
io.on('connection', (socket) => {
  socket.on('change room', (data) => {
    var size = 0;
    var room = io.sockets.adapter.rooms[data.roomid]; // send users of room
    var check = true;
    //console.log(room,'rooms',io.sockets.adapter.rooms);
    //console.log(room)
    if(room){
      for (var item in room.sockets) {
        if (room.sockets.hasOwnProperty(item) && item != socket.id) size++;
      }
      if(size == 2){
        check = false;
      } 
    }
    if(size == 0){
      users[data.roomid] = data.username;
    }
    if(check) {
      for (var item in socket.rooms) {
          console.log(item,'rooms')
          socket.leave(item);
          delete users[item];
        }
      
       socket.join(data.roomid);
          socket.broadcast.to(data.roomid).emit('update user',{username:data.username,size:size});
          socket.emit('change room',{status:true,size:size,id:data.roomid,username:users[data.roomid]});
       console.log(data.username,'username')
    } else {
       socket.emit('change room',{status:false,size:size});
     }
  })
  socket.on('player action', (data) => {
  	
    socket.broadcast.to(data.roomid).emit('player action',data);
    //socket.broadcast.emit('player action',data);
  })
  socket.on('restart game', (data) => {
    socket.broadcast.to(data.roomid).emit('restart game',false);

  })

  socket.on('update username', (data) => {
    socket.username = data;
    var room;
  for (var item in socket.rooms) {
      room = socket.rooms[item];
  }
  users[room] = data;
  socket.emit('get userroom',room);
    //socket.broadcast.emit('player action',data);
  })



});
